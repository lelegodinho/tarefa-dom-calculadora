const botaoAD = document.querySelector("#ad")
const botaoCM = document.querySelector("#cm")
const tela = document.querySelector("#tela")
const media = document.querySelector("#media")

var lista =[]
var num = 0
var input = ""


botaoAD.addEventListener("click", () => {
    var input = document.querySelector("#nota").value;

    if(input == []) {
        alert("Por favor, insira uma nota.");
    } 
    else if ((isNaN(parseFloat(input)) == true)) {
        alert("A nota digitada é inválida, por favor, insira uma nota válida.");
    } 
    else if (input < 0 || input > 10) {
        alert("A nota digitada é inválida, por favor, insira uma nota válida.");
    }
    else {
        lista.push(parseFloat(input));
        num++;

        var frase = "A nota " + num + " foi " + input
        tela.innerHTML = tela.innerHTML + frase + "\n"; 
    }
})

botaoCM.addEventListener("click", () => {
    var soma = lista.reduce((soma, i) => soma + i);
    soma = soma / num;
    media.innerHTML = soma.toFixed(2);
})